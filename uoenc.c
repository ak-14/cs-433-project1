#include "utils.h"
#include "parse.h"
#include "network.h"


int main(int argc, char **argv) 
{
    FILE *input_file;
    FILE *output_file;
    char *input_filename; 
    char *output_filename;

    int lflag = 0;
    char *dvalue = NULL;

    size_t read_ret = 0;
    size_t total_bytes = 0;

    char *block = (char*) malloc (BLOCK_SIZE + PAD_SIZE);
    char *hash = malloc(HMAC_SIZE);

    size_t passlen;
    char *password = malloc(PASSWORD_SIZE);
    void *salt = malloc(SALT_SIZE);
    void *key = malloc(KEY_SIZE);
    unsigned char *iv = malloc(IV_SIZE);

    gcry_cipher_hd_t *cipher_hd = malloc(sizeof(gcry_cipher_hd_t));
    gcry_md_hd_t *md_hd = malloc(sizeof(gcry_md_hd_t));


    /* initialize glibcrypt */
    init_glibcrypt();


    /* parse arguments */
    if (parse_uoenc(argc, argv, &input_filename, &lflag, &dvalue) != 0)
        return -1;


    /* quit if no file is given */
    if (input_filename == NULL)
        return -1;


    /* derive output file name */
    output_filename = malloc(strlen(input_filename) + strlen(".uo"));
    strcpy(output_filename, input_filename);
    strcat(output_filename, ".uo");


    /* exit if file exists */
    if (access (output_filename, F_OK) == 0)
    {
        fprintf(stderr, "File %s exists\n", output_filename);
        return -1;
    }


    /* open input and output files */
    input_file = fopen (input_filename , "rb");
    output_file = fopen (output_filename, "wb");


    /* prompt for password */
    passlen = prompt_password(password);


    /* derive a key from password and store it in key buffer */
    derive_key (password, passlen, salt, 0, key);


    /* generate an iv */
    gcry_create_nonce (iv, IV_SIZE);


    /* initialize the cipher handler with key and iv */
    cipher_init (cipher_hd, key, KEY_SIZE, iv, IV_SIZE);


    /* write salt and iv to the output file */
    fwrite (salt , 1, SALT_SIZE, output_file);
    fwrite (iv , 1, IV_SIZE, output_file);


    /* add salt and iv to hmac handler by passing NULL for hash */
    hmac (md_hd, key, KEY_SIZE, salt, SALT_SIZE, NULL);
    hmac (md_hd, NULL, 0, iv, IV_SIZE, NULL);


    /* read the input file in blocks */
    while ( (read_ret = fread (block, 1, BLOCK_SIZE, input_file)) > 0 ) 
    {  
        printf ("read %d bytes, ", read_ret);


        /* add padding */
        read_ret += pad (block, read_ret);


        /* encrypt a block of data in the block buffer*/
        gcry_cipher_encrypt (*cipher_hd, block, read_ret, NULL, 0);


        /* add block to hmac handler by passing NULL for hash */    
        hmac (md_hd, NULL, 0, block, read_ret, NULL);


        /* write a cipher block to the file */
        fwrite (block , 1, read_ret, output_file);


        printf ("wrote %d bytes\n", read_ret);
        total_bytes += read_ret;
    }

    /* read hmac into the buffer hash */    
    hmac (md_hd, NULL, 0, NULL, 0, hash);


    /* write hmac to the file and close open files */
    fwrite (hash , 1, HMAC_SIZE, output_file);
    fclose (output_file);
    fclose (input_file);


    printf ("Successfully encrypted %s to %s(%d bytes written).\n", 
            input_filename, output_filename, total_bytes);


    /* if d flag is set, send to destination */
    if (dvalue != NULL)
    {
        printf("Transmitting to %s\n", dvalue);
        char *ip = strtok(dvalue, ":");
        int port = atoi(strtok(NULL, ":"));
        send_data (ip, port, output_filename);
        remove (input_filename);
        printf("Successfully received\n");
    }

    /* clean up */
    free (block);
    free (hash);
    free (password);
    free (salt);
    free (key);
    free (iv);
    gcry_cipher_close (*cipher_hd);

    return 0;
}
