#ifndef __PARSE_H__
#define __PARSE_H__

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
 
int parse_uoenc (int argc, char **argv, char **filename, int *lflag, char **dvalue);

int parse_uodec (int argc, char **argv, char **port, char **lvalue);

#endif