#include "utils.h"
#include "parse.h"
#include "network.h"


int main (int argc, char **argv)
{
    FILE *input_file;
    FILE *output_file;
    char *input_filename = malloc(FILE_SIZE); 
    int size = 0;
    char *output_filename;

    char *lvalue = NULL;
    char *port;

    long file_size;
    size_t total_bytes = 0;
    size_t read_ret;

    char *block = malloc(BLOCK_SIZE + PAD_SIZE);
    char *hash = malloc(HMAC_SIZE);
    char *recv_hmac = malloc(HMAC_SIZE);

    size_t passlen;
    char *password = malloc(PASSWORD_SIZE);
    void *salt = malloc(SALT_SIZE);
    void *key = malloc(KEY_SIZE);
    unsigned char *iv = malloc(IV_SIZE);

    gcry_cipher_hd_t *cipher_hd = malloc(sizeof(gcry_cipher_hd_t));
    gcry_md_hd_t *md_hd = malloc(sizeof(gcry_md_hd_t));


    /* initialize glibcrypt */
    init_glibcrypt();


    /* parse arguments */
    if (parse_uodec(argc, argv, &port, &lvalue) != 0)
        return -1;


    /* get file name if local mode is set or get from network */
    if (lvalue != NULL)
    {
        strcpy(input_filename, lvalue);
        size = strlen(input_filename);
    }
    else
    {
        printf("Waiting for connections.\nInbound file. ");
        if (port == NULL)
            size = recv_data (8888, input_filename);
        else
            size = recv_data (atoi(port), input_filename);
    }

    output_filename = malloc(size);

    strncpy(output_filename, input_filename, size - strlen(".uo"));


    /* wait for file to be deleted */
    sleep(2);


    /* exit if file exists */
    if (access (output_filename, F_OK) == 0)
    {
        fprintf(stderr, "File %s exists\n", output_filename);
        return -1;
    }


    /* open input and output files */
    input_file = fopen (input_filename , "rb");

    output_file = fopen (output_filename, "wb");


    /* prompt for password */
    passlen = prompt_password(password);


    /* obtain file size */
    fseek (input_file , 0 , SEEK_END);
    file_size = ftell (input_file);
    rewind (input_file);


    /* read salt and iv */
    fread (salt, 1, SALT_SIZE, input_file);
    fread (iv, 1, IV_SIZE, input_file);


    /* derive a key from password and store it in key block */
    derive_key(password, passlen, salt, SALT_SIZE, key);


    /* add salt and iv to hmac handler by passing NULL for hash */
    hmac (md_hd, key, KEY_SIZE, salt, SALT_SIZE, NULL);
    hmac (md_hd, NULL, 0, iv, IV_SIZE, NULL);


    /* determine cipher text size and number of blocks */
    int cipher_size = file_size - (HMAC_SIZE + SALT_SIZE + IV_SIZE);
    int blocks = cipher_size / (BLOCK_SIZE + PAD_SIZE);


    /* TODO make a function to read blocks of data */
    int lastblock;
    while (blocks >= 0)
    {
        blocks -= 1;
        lastblock = (file_size - HMAC_SIZE) - ftell (input_file);

        if (lastblock > BLOCK_SIZE)
            read_ret = fread (block, 1, BLOCK_SIZE + PAD_SIZE, input_file);
        else
            read_ret = fread (block, 1, lastblock, input_file);

        hmac (md_hd, NULL, 0, block, read_ret, NULL);
    }

    /* compute hmac and store it in buffer hash */
    hmac (md_hd, NULL, 0, NULL, 0, hash);


    /* read the received hmac into the buffer recv_hmac */
    fread (recv_hmac, 1, HMAC_SIZE, input_file);


    /* exit if hmac is not valid */
    if (memcmp (hash, recv_hmac, HMAC_SIZE) != 0) 
        return -1;


    /* seek to the beginning of the cipher text */
    fseek (input_file, SALT_SIZE + IV_SIZE, SEEK_SET);


    /* initialize the cipher handler with key and iv */
    cipher_init (cipher_hd, key, KEY_SIZE, iv, IV_SIZE);


    /* read the input file in blocks and decrypt */
    blocks = cipher_size / (BLOCK_SIZE + PAD_SIZE);
    while (blocks >= 0)
    {
        blocks -= 1;
        lastblock = (file_size - HMAC_SIZE) - ftell (input_file);

        if (lastblock > BLOCK_SIZE)
            read_ret = fread (block, 1, BLOCK_SIZE + PAD_SIZE, input_file);
        else
            read_ret = fread (block, 1, lastblock, input_file);


        printf ("read %d bytes, ", read_ret);


        /* decrypt a block of data in block */
        gcry_cipher_decrypt (*cipher_hd, block, read_ret, NULL, 0);


        /* unpad the block */
        int pad_size = unpad (block, read_ret);


        /* write decrypted block to the file */
        fwrite (block , 1, read_ret - pad_size, output_file);


        printf ("wrote %d bytes\n", read_ret - pad_size);
        total_bytes += read_ret;
    }
     printf ("Successfully received and decrypted %s to %s(%d bytes written).\n", 
            input_filename, output_filename, total_bytes);


    /* clean up */
    fclose (input_file);
    fclose (output_file);
    free (block);
    free (hash);
    free (recv_hmac);
    free (password);
    free (salt);
    free (key);
    free (iv);
    gcry_cipher_close (*cipher_hd);

    return 0;
}
