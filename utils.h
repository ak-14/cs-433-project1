#ifndef __UTILS_H__
#define __UTILS_H__

#include <stdio.h>
#include <gcrypt.h>

#define FILE_SIZE 100
#define BLOCK_SIZE 1024
#define PAD_SIZE 16

#define PASSWORD_SIZE 100
#define SALT_SIZE 32
#define KEY_SIZE 32
#define ITERATIONS 1000
#define IV_SIZE 16
#define HMAC_SIZE 32

void init_glibcrypt ();

void cipher_init (gcry_cipher_hd_t *hd,  void *key, size_t keylen, const void *iv, size_t ivlen);

void derive_key (void *passphrase, size_t passlen, void *salt, size_t saltlen, void *key);

void hmac (gcry_md_hd_t *hd, void *key, size_t keylen, void *block, size_t block_size, void *hash);

size_t prompt_password (char *buffer);

int pad (char *block, size_t pad_size);

int unpad (char *block, size_t pad_size);

#endif