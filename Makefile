CFLAGS= -Wall
CC= gcc
SRCS=uoenc.c uodec.c utils.c parse.c network.c
OBJS=uoenc.o uodec.o utils.o parse.o network.o
LIBS=utils.o parse.o network.o
LDFLAGS= -g
LIBGCRYPT= `libgcrypt-config --cflags --libs`


all:      uoenc uodec

uoenc: $(OBJS)
	$(CC) $(LIBS) uoenc.o -o uoenc $(LIBGCRYPT)

uodec: $(OBJS)
	$(CC) $(LIBS) uodec.o -o uodec $(LIBGCRYPT) 

$(OBJS):  $(SRCS)
	$(CC) -c $*.c $(LIBGCRYPT) 

clean:
	rm -f *.o


#valgrind --tool=memcheck --leak-check=full --show-reachable=yes --num-callers=20 --track-fds=yes ./uoenc
