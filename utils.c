#include "utils.h"


void init_glibcrypt()
{
    /* source: http://www.gnupg.org/documentation/manuals/gcrypt */

    /* Version check should be the very first call because it
     * makes sure that important subsystems are initialized. */
    if (!gcry_check_version (GCRYPT_VERSION))
    {
        fputs ("libgcrypt version mismatch\n", stderr);
        exit (2);
    }

    /* We don't want to see any warnings, e.g. because we have not yet
     * parsed program options which might be used to suppress such
     * warnings. */
    gcry_control (GCRYCTL_SUSPEND_SECMEM_WARN);

    /* ... If required, other initialization goes here.  Note that the
     * process might still be running with increased privileges and that
     * the secure memory has not been initialized.  */

    /* Allocate a pool of 16k secure memory.  This make the secure memory
     * available and also drops privileges where needed.  */
    gcry_control (GCRYCTL_INIT_SECMEM, 16384, 0);

    /* It is now okay to let Libgcrypt complain when there was/is
     * a problem with the secure memory. */
    gcry_control (GCRYCTL_RESUME_SECMEM_WARN);

    /* ... If required, other initialization goes here.  */

    /* Tell Libgcrypt that initialization has completed. */
    gcry_control (GCRYCTL_INITIALIZATION_FINISHED, 0);
}


void cipher_init (gcry_cipher_hd_t *hd,  void *key, size_t keylen, const void *iv, size_t ivlen)
{
    gcry_cipher_open (hd, GCRY_CIPHER_AES256, GCRY_CIPHER_MODE_CBC, 0);

    gcry_cipher_setkey (*hd, key, keylen);

    gcry_cipher_setiv (*hd, iv, ivlen);
}


void derive_key (void *passphrase, size_t passlen, void *salt, size_t saltlen, void *key)
{
    if (saltlen == 0)
        gcry_randomize(salt, SALT_SIZE, GCRY_STRONG_RANDOM);

    gcry_kdf_derive (passphrase, passlen, GCRY_KDF_PBKDF2, GCRY_MD_SHA512,
            salt, SALT_SIZE, ITERATIONS, KEY_SIZE, key);
}


void hmac (gcry_md_hd_t *hd, void *key, size_t keylen, void *block, size_t block_size, void *hash)
{
    /* pass key when first initializing the digest handle and NULL afterwards */
    if (key != NULL)
    {
        gcry_md_open (hd, GCRY_MD_SHA256, GCRY_MD_FLAG_HMAC);
        gcry_md_setkey (*hd, key, keylen);
    }

    if (block != NULL)
        gcry_md_write(*hd, block, block_size);

    /* pass a buffer along with the last block to store the result in and close the handle */
    if (hash != NULL)
    {
        memcpy(hash, gcry_md_read(*hd, 0), HMAC_SIZE);
        gcry_md_close(*hd);
    }
}


size_t prompt_password (char *buffer)
{
    size_t len;

    fputs("Password: ", stdout);

    fgets(buffer, PASSWORD_SIZE, stdin);

    len = strlen(buffer);

    return len;
}


int pad(char *block, size_t block_size)
{
    int i;
    size_t pad_size;

    pad_size = PAD_SIZE - (block_size % PAD_SIZE);

    for (i = block_size; i <= block_size + pad_size; i++)
        *(block + i) = pad_size;

    return pad_size;
}


int unpad(char *block, size_t block_size)
{
    size_t pad_size;

    pad_size = *(block + block_size - 1);

    return pad_size;
}

