#include "parse.h"

/* based on code from: 
http://www.gnu.org/software/libc/manual/html_node/Example-of-Getopt.html#Example-of-Getopt */

int parse_uoenc (int argc, char **argv, char **filename, int *lflag, char **dvalue)
{
    int c;

    opterr = 0;

    if (argc > 4)
    {
        fprintf(stderr, "Too many arguments\n");
        return 1;
    }

    while ((c = getopt (argc, argv, "ld:")) != -1)
        switch (c)
        {
            case 'l':
                *lflag = 1;
                break;
            case 'd':
                *dvalue = optarg;
                break;
            case '?':
                if (optopt == 'd')
                    fprintf (stderr, "Option -%c requires an argument.\n", optopt);
                else if (isprint (optopt))
                    fprintf (stderr, "Unknown option `-%c'.\n", optopt);
                else
                    fprintf (stderr,
                            "Unknown option character `\\x%x'.\n",
                            optopt);
                return 1;
            default:
                abort ();
        }

    *filename = argv[optind];

    return 0;
}


int parse_uodec (int argc, char **argv, char **port, char **lvalue)
{
    int c;

    opterr = 0;

    if (argc > 3)
    {
        fprintf(stderr, "Too many arguments\n");
        return 1;
    }
    while ((c = getopt (argc, argv, "l:")) != -1)
        switch (c)
        {
            case 'l':
                *lvalue = optarg;
                break;
            case '?':
                if (optopt == 'l')
                    fprintf (stderr, "Option -%c requires an argument.\n", optopt);
                else if (isprint (optopt))
                    fprintf (stderr, "Unknown option `-%c'.\n", optopt);
                else
                    fprintf (stderr,
                            "Unknown option character `\\x%x'.\n",
                            optopt);
                return 1;
            default:
                abort ();
        }

    *port = argv[optind];

    return 0;
}


