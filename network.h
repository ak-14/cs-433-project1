#ifndef __NETWORK_H__
#define __NETWORK_H__

#include <stdio.h> 
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <resolv.h>
#include <arpa/inet.h>

#define BUFFER_SIZE 1024 

int send_date (char *ip, int port, FILE *fp, char *filename);

int recv_data ();

#endif