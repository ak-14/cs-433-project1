#include "network.h"


int send_data (char *ip, int port, const char *filename)
{
    int socket_fd;
    struct sockaddr_in address;
    char buffer [BUFFER_SIZE];

    socket_fd = socket(AF_INET, SOCK_STREAM, 0);

    /* initialize server address/port struct */
    bzero(&address, sizeof(address));

    address.sin_family = AF_INET;

    address.sin_port = htons(port);

    inet_aton(ip, &address.sin_addr.s_addr);

    /* connect to server */
    connect(socket_fd, (struct sockaddr*)&address, sizeof(address));

    /* send filename */
    send(socket_fd, filename, (int)strlen(filename), 0);

    int read_ret;

    FILE *fp = fopen(filename, "rb");

    while ( (read_ret = fread(buffer, 1, BUFFER_SIZE, fp)) > 0)
        send(socket_fd, buffer, read_ret, 0);

    /* clean up */
    close (socket_fd);
    fclose (fp);
    
    return 0;
}


int recv_data (int port, char* filename)
{
    int sockfd;
    struct sockaddr_in self;
    char buffer[BUFFER_SIZE];

    /* create streaming socket */
    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    /* initialize address/port structure */
    bzero(&self, sizeof(self));
    self.sin_family = AF_INET;
    self.sin_port = htons(port);
    self.sin_addr.s_addr = INADDR_ANY;

    /* assign a port number to the socket */
    bind(sockfd, (struct sockaddr*)&self, sizeof(self));
 
    /* make it a "listening socket" */
    listen(sockfd, 20);

    int clientfd;
    struct sockaddr_in client_addr;
    int addrlen = sizeof(client_addr);

    /* accept a connection (creating a data pipe) */
    clientfd = accept(sockfd, (struct sockaddr*)&client_addr, &addrlen);

    bzero(buffer, BUFFER_SIZE);

    bzero(filename, 100);

    int filename_size = recv(clientfd, filename, 100, 0);

    FILE * outf = fopen("tmp", "wb");

    int read_ret;
    while( (read_ret = recv (clientfd, buffer, BUFFER_SIZE, 0)) > 0)
        fwrite(buffer, 1, read_ret, outf);

    close(clientfd);    
    close(sockfd);
    fclose(outf);

    remove(filename);
    rename("tmp", filename);

    return filename_size;
}
